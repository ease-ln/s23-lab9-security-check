# Lab9 -- Security check

## Website: https://snapcraft.io/

### Forgot password check
 ------ 
##### Requirenment: Return a consistent message for both existent and non-existent accounts.
Test  case 1: Return a consistent message for non-existent account

|What Done|Status|Comment|
|---|---|---|
|Open website https://snapcraft.io/|OK|---|
|Click Sign in|OK|---|
|Click 'Forgot your password?'|OK|---|
|Enter non-existing password|OK| e.g arina.drenyassova@gmail.com|
|Pass 'I'm not a robot captcha|OK|---|
|Press continue button|OK|---|
|Return consistent message|Fail| redirect to https://login.ubuntu.com/+forgot_password for details, No message that account with email absent in database|
* Result: Fail, mo message that account with email absent in system

Test  case 2: Return a consistent message for existent account
|What Done|Status|Comment|
|---|---|---|
|Open website https://snapcraft.io/|OK|---|
|Click Sign in|OK|---|
|Click 'Forgot your password?'|OK|---|
|Enter existing password|OK| e.g a.drenyassova@gmail.com|
|Pass 'I'm not a robot captcha|OK|---|
|Press continue button|OK|---|
|Return consistent message|OK| redirect to https://login.ubuntu.com/+forgot_password for details|
* Result: Success, despite same page for existent and non-existent accounts message info is valid and clear
 ------

##### Requirenment: Ensure that generated tokens or codes are:
- Randomly generated using a cryptographically safe algorithm.
- Sufficiently long to protect against brute-force attacks.
- Single use and expire after an appropriate period.

Test case 3: Ensure token generated
|What Done|Status|Comment|
|---|---|---|
|Test case 2|OK|all steps from test case 2|
|Message send to email|OK|--|
|URL with token provided|OK| token sufficiently long, e.g https://login.ubuntu.com/token/qHnMF4WKzCJF6RmfRQ73/+resetpassword/a.drenyassova@gmail.com|
|Test case 2|OK|all steps from test case 2|
|URL with token provided|OK| every try token is changed, https is used, adds the Referrer Policy tag  e.g https://login.ubuntu.com/token/FJ7kRPcQKmcyWYgVXjmP/+resetpassword/a.drenyassova@gmail.com|
|Enter old pass |FAIL| no warning that last password can not be the same as new one |
|Enter new pass |FAIL| successfully changed and logins client to the server, which opens gate to vulnerabilities according to OWASP CSS|
|Try old link|OK| single use and expire after an appropriate period. Redirect to https://login.ubuntu.com/+bad-token |
* Result: Success, despite same page for existent and non-existent accounts message info is valid and clear
 ------ 

##### Requirenment: Do not make a change to the account until a valid token is presented, such as locking out the account
Test case 4: No account locking out
|What Done|Status|Comment|
|---|---|---|
|Open website https://snapcraft.io/|OK|---|
|Click Sign in|OK|---|
|Click 'Forgot your password?'|OK|---|
|Enter existing password|OK| e.g arina.drenyassova@gmail.com|
|Pass 'I'm not a robot captcha|OK|---|
|Press continue button|OK|---|
|Open URL from mail|OK|---|
|In new tab open website https://snapcraft.io/|OK|---|
|Click Sign in|OK|---|
|Click 'Forgot your password?'|OK|---|
|Enter existing password|OK| e.g arina.drenyassova@gmail.com|
|Pass 'I'm not a robot captcha|OK|---|
|Press continue button|OK|---|
|Open URL from mail|OK|---|
|Enter new|OK| change password|
|Enter new in initial tab |OK| redirect to https://login.ubuntu.com/token/nyxGb3Yq33tvRQFwNDjj/+resetpassword/a.drenyassova@gmail.com|
* Result: Success, no locking out
